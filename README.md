# How to compile files after you modify SCSS Files #

npm install  
gulp build-css

# How to run sendEmail.js

sendEmail.js is a node worker that is written with express.js, when you execute it, it runs on port 3000, has one single endpoint : **...:300/apply** . 

when you set it up on your own server change the following URL to the one that matches your IP Server

```javascript

this.$http.post('http://127.0.0.1:3000/apply', this.form).then(function(r)

```


all the packages that are required for sendEmail to work properly are written as dependencies in **package.json**, so when you run *npm install*, it installs all the packages, you can simply follow with the commande **node sendEmail.js** or set up a deamon worker that keeps the server running forever
You can achieve that by installing *https://github.com/foreverjs/forever*
