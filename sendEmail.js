const express = require('express');
const app = express();
const port = 3000
var cors = require('cors');

var mailgun = require("mailgun-js");
var api_key = 'key-2fc72703b6bc193d3b0b0f888a0afc27';
var DOMAIN = 'sandbox7a0617f7c7d24a918bae70ea6ca97240.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});

app.use(cors())

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


const { check, validationResult } = require('express-validator/check');

app.post('/apply', [
    check('email').isEmail().withMessage('Must be a valid email'),
    check('profile').isURL().withMessage('Must be a valid URL'),
    check('name').isLength({ min: 5 }).withMessage('Must be at least 5 chars long'),
    check('purpose').isLength({ min: 2 }).withMessage('Please select your perefered job position')
], (req, res) => {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    console.log(req.body);

    var data = {
        from: req.body.name + ' <jobs@aspirible.com>',
        to: 'capcashmere@gmail.com, romymisra@gmail.com',
        subject: 'Aspirible Job Application',
        text: 'Name : ' + req.body.name + '\n'
               + 'Email : ' + req.body.email + '\n'
               + 'Prefered Job Position : ' + req.body.purpose + '\n'
               + 'Online profile : ' + req.body.profile + '\n'
               + 'Additional info : ' + req.body.additionalText + '\n'
    };

    mailgun.messages().send(data, function (error, body) {
        console.log(body);

        res.send({success: true});

    });


    

});

app.listen(port, () => console.log(`Sending app listening on port ${port}!`))